(function(dice, discord, undefined) {
    console.log(dice.rk.roll(8,3));
    console.log(dice.cd8.roll(8, 10));
    console.log(dice.cd8.roll(8));
    console.log(dice.mg.roll(5, 8));
    console.log(dice.wu.roll(5));
    console.log(dice.wu.roll(2, "defense"));
    console.log(dice.wu.roll(2, "spell"));


    console.log(dice.gy.roll(2, 6, 0));
    console.log(dice.gy.roll(3, 10, 2));
    console.log(dice.gy.roll(1, 20));
    console.log(dice.gy.roll(8));

    console.log(dice.surge.roll(3));
    console.log(dice.surge.roll(4));
    console.log(dice.surge.roll(5));

    console.log(dice.fw.roll(1, 2));
    console.log(dice.fw.roll(2, 2));
    console.log(dice.fw.roll(3, 2));
    console.log(dice.fw.roll(15, 2));
    console.log(dice.fw.roll(15, 8));
    console.log(dice.fw.roll(9, 8));

})(require("./dice_systems.js"),
   undefined);
