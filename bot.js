(function(console, proc, dumps, dice, discord, config, undefined) {
    function abort(msg) {
        console.error(msg);
        return proc.exit(1);
    }

    function get_nth(sequence, index) {
        if(index<0 || index>sequence.length) {
            return undefined;
        }
        return sequence[index];
    }

    function normalize_num(number) {
        return (number>1000) ? 1000 : number;
    }

    function extract_num(numTxt) {
        try {
            return normalize_num(parseInt(numTxt));
        } catch(e) {
            console.error(`Failed to parse ${numTxt}: ${e}`);
            return undefined;
        }
    }

    function num_frag(fragment, pattern) {
        return (function(subfrag) {
            return extract_num(get_nth(subfrag.match(/[0-9]+/g), 0));
        })(fragment.match(pattern));
    }

    function sanitize_r(rollRaw) {
        return rollRaw.replace(/\D+/g, function(match) {
            return " "+match;
        });
    }

    function safe_apply(func, numbersRaw) {
        return (function(numbers) {
            return func.apply(null, numbers);
        })(sanitize_r(numbersRaw).match(/[\d-]+/g).map(extract_num));
    }

    function mg_roll(msg) {
        return msg.reply(safe_apply(dice.mg.roll, msg.content));
    }

    function rk_roll(msg) {
        return msg.reply(safe_apply(dice.rk.roll, msg.content));
    }

    function cd8_roll(msg) {
        return msg.reply(safe_apply(dice.cd8.roll, msg.content));
    }

    function gy_roll(msg) {
        return msg.reply(safe_apply(dice.gy.roll, msg.content));
    }

    function surge_roll(msg) {
        return msg.reply(safe_apply(dice.surge.roll, msg.content));
    }

    function fw_roll(msg) {
        return msg.reply(safe_apply(dice.fw.roll, msg.content));
    }

    function whu_roll_attack(msg) {
        return msg.reply();
    }

    function whu_roll_defense(msg) {
        return msg.reply();
    }

    function whu_roll_spell(msg) {
        return msg.reply();
    }

    function select_handler(contentRaw) {
        return (function(content) {
            if(content.match(/^\/rk/g))
                return rk_roll;
            if(content.match(/^\/mg/g))
                return mg_roll;
            if(content.match(/^\/cosmo/g))
                return cd8_roll;
            if(content.match(/^\/attack/g))
                return whu_roll_attack;
            if(content.match(/^\/defense/g))
                return whu_roll_defense;
            if(content.match(/^\/spell/g))
                return whu_roll_spell;
            if(content.match(/^\/help/g))
                return show_help;
            if(content.match(/^\/.*[0-9]*d[0-9]/g))
                return gy_roll;
            if(content.match(/\/fw/g))
                return fw_roll;
            if(content.match(/\/s/g))
                return surge_roll;
            return (function(msg) {});
        })(contentRaw.toLowerCase());
    }

    function show_help(msg) {
        if(msg.content === "/help gy")
            return msg.reply("Standard polyhedral dice of the Gygaxian variety.");
        if(msg.content === "/help rk")
            return msg.reply("L5R's roll/keep system.  Call as `/rk XkY` where `X` is the number of dice rolled and `Y` is the number kept.");
        if(msg.content === "/help mg")
            return msg.reply("Moon's Grave's D12 system.  Call as `/mg x y` where `x` is the number of dice rolled and `y` is the RT.");
        if(msg.content === "/help cosmo")
            return msg.reply("Cosmoforce cascading D8 system.  Call as `/cosmo x` or `/cosmo x y`.  `x` is the number of dice rolled and optional `y` is the mass of the target you're shooting. ");
        if(msg.content === "/help wu")
            return msg.reply("Warhammer Underworlds dice rolling.  Call as `/attack x` where `x` is the number of dice rolled `/defense x`, or `/spell x`");

        return msg.reply("Type `/help <system>` for help on how to roll dice for a system.\nSystems include `rk`, `mg`, `cosmo`, `gy`, `wu`");
    }

    return (function(client) {
        client.on("ready", function(info) {
            console.log("Bot is running...");
        });

        client.on("message", function(msg) {
            select_handler(msg.content)(msg);
        });

        client.login(config.token);

    })(new discord.Client());
})(console,
   process,
   function(obj) {
       try {
           return JSON.stringify(obj, null, 2);
       } catch(e) {
           return obj.toString();
       }
   },
   require("./dice_systems.js"),
   require("discord.js"),
   require("./config.js"));
