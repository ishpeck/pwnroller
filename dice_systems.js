((function(module, console, math, undefined) {
    "use strict";

    function limited_number(minval, maxval, gotval) {
        if(!gotval || gotval<minval)
            return minval;
        if(gotval>maxval)
            return maxval;
        return gotval;
    }

    function rng0to1000(num) {
        return limited_number(0, 1000, num);
    }

    function rng1to1000(num) {
        return limited_number(1, 1000, num);
    }

    function rng2to1000(num) {
        return limited_number(2, 1000, num);
    }

    function sumup(list) {
        try {
            return list.reduce(function(a,b) {
                return a+b;
            });
        } catch(e) {
            console.error("Failed to numericalize list!");
            console.error(JSON.stringify(list));
            return 0;
        }
    }

    function dice_pool(quantity, random, shallCascade) {
        var collection = [];
        var protectionAmount = 1000;
        var dieVal;
        while(quantity-- > 0 && protectionAmount-- > 0) {
            dieVal = random();
            if(shallCascade(dieVal))
                ++quantity;
            collection.push(dieVal);
        }
        return collection;
    }

    var l5rRollKeepSystem = (function() {
        function random() {
            return math.floor((math.random() * 10) + 1);
        }

        function repr_die(item) {
            if(item.length > 1)
                return "("+item.join("+")+")"
            return item.toString();
        }

        function rk_sort_dice_ascending(left, right) {
            return (function(leftSum, rightSum) {
                return leftSum - rightSum;
            })(sumup(left), sumup(right));
        }

        function rk_sort_dice_descending(left, right) {
            return (function(leftSum, rightSum) {
                return rightSum - leftSum;
            })(sumup(left), sumup(right));
        }

        function rk_roll_single_die(explodeOn) {
            var thisDie = [];
            var thisVal;
            do {
                thisVal = random();
                thisDie.push(thisVal);
            } while(thisVal >= explodeOn);
            return thisDie;
        }

        function rk_repr_roll(kept, dropped) {
            return `${kept.length+dropped.length}k${kept.length}: ` +
                `**${sumup(kept.map(sumup)).toString()}** = ` +
                `${kept.map(repr_die).join("+")} | ` +
                `${dropped.map(repr_die).join(",")}`;
        }

        function rk_roll_dice(toRoll, toKeep, sortMethod, explodeOn) {
            var collection = [];
            while ( toRoll-- > 0 )
                collection.push(rk_roll_single_die(explodeOn));
            collection.sort(sortMethod);
            return rk_repr_roll(collection.slice(0, toKeep),
                                collection.slice(toKeep));
        }

        function l5r_explosion_guard(num) {
            if(!num)
                return 10;
            return rng2to1000(num);
        }

        function l5r_select_sort_mthd(method) {
            return (method !== 1) ?
                rk_sort_dice_descending :
                rk_sort_dice_ascending;
        }

        function roll_dice(toRoll, toKeep, explodeOnRaw, sortMethodRaw) {
            return rk_roll_dice(rng1to1000(toRoll),
                                rng1to1000(toKeep),
                                l5r_select_sort_mthd(sortMethodRaw),
                                l5r_explosion_guard(explodeOnRaw));
        }

        return {
            "roll": roll_dice,
            "name": "Roll/keep system",
            "description": "The roll/keep system as popularized by L5R.",
        };
    })();

    var cosmoforceD8 = (function(drossVal) {
        function random() {
            return math.floor((math.random() * 8) + 1);
        }

        function roll_dice(quantity, margin, cascadingValue) {
            var retainedIdx = 0;
            if(!quantity)
                return "You must specify how many dice to roll and it must be a number between 1 and 100 (inclusive).";
            if(quantity<1)
                return "There's no point in rolling fewer than 1 dice.";
            cascadingValue = cascadingValue || 1;
            var collection = dice_pool(quantity, random, function(dieVal) {
                return dieVal <= cascadingValue;
            });
            var denote = `${quantity}in${margin}`;
            if(!margin || margin<1) {
                margin = 10000;
                denote = `${quantity}dice`;
            }
            collection.sort(function(left, right) {
                return left - right;
            });

            while(retainedIdx<collection.length &&
                  collection[retainedIdx]<drossVal &&
                  sumup(collection.slice(0, retainedIdx+1)) <= margin)
                ++retainedIdx;
            var effective = collection.slice(0, retainedIdx);
            var dross = collection.slice(retainedIdx);
            
            return `${denote}: **${effective.length}** => ` +
                `${effective.join(",")} | ` +
                `${dross.join(",")}`;
        }

        return {
            "roll": roll_dice,
            "name": "Cascading D8 system.",
            "description": "The cascading D8 system of Cosmoforce."
        };
    })(7);

    var moonsGrave = (function() {
        function random() {
            return math.floor((math.random() * 12) + 1);
        }

        function mg_roll_dice(quantity, rollTarget, spikeOn) {
            var scoreIdx = 0;
            var floatingBonuses = 0;
            var denote = `${quantity}rt${rollTarget}`;
            var collection = dice_pool(quantity, random, function(dieVal) {
                if(dieVal < spikeOn)
                    return false;
                ++floatingBonuses;
                return true;
            });
            collection.sort(function(left, right) {
                return right - left;
            });

            var scoring = [];
            while(scoreIdx < collection.length &&
                  collection[scoreIdx]+floatingBonuses >= rollTarget ) {
                var curIdx = scoreIdx++;
                if(collection[curIdx] < rollTarget) {
                    var diff = rollTarget-collection[curIdx];
                    floatingBonuses -= diff;
                    scoring.push(`${collection[curIdx].toString()}+${diff}`);
                    continue;
                }
                scoring.push(collection[curIdx].toString());
            }

            return `${denote}: Score **${scoring.length}** => ${scoring.join(",")} | ${collection.slice(scoreIdx).join(",")}`;
        }

        function roll_dice(quantity, rollTarget, spikeOn) {
            return mg_roll_dice(
                rng1to1000(quantity),
                rng0to1000(quantity),
                spikeOn || 12);
        }

        return {
            "roll": roll_dice,
            "name": "Moon's Grave dice system.",
            "description": "The d12-based Moon's Grave dice mechanic."
        };
    })();

    var warhammerUnderworlds = (function() {
        function random() {
            return math.floor((math.random() * 6));
        }

        function never(x) {
            return false;
        }

        function toss_dice(quantity) {
            var collection = dice_pool(quantity, random, never);
            collection.sort(function(left, right) {
                return left - right;
            });
            return collection;
        }

        function roll_spell_dice(quantity) {
            return `Spell: `+toss_dice(quantity).map(function(facing) {
                if(facing==0)
                    return ":grey_expclamation:";
                if(facing<4)
                    return ":zap:";
                return ":cyclone:";
            }).join(" ");
        }

        function roll_defense_dice(quantity) {
            return `Defense: `+toss_dice(quantity).map(function(facing) {
                if(facing==0)
                    return ":grey_exclamation:";
                if(facing<3)
                    return ":shield:";
                if(facing<4)
                    return ":leftwards_arrow_with_hook:";
                if(facing<5)
                    return ":last_quarter_moon:";
                return ":new_moon:";
            }).join(" ");
        }

        function roll_attack_dice(quantity) {
            return `Attack: `+toss_dice(quantity).map(function(facing) {
                if(facing==0)
                    return ":exclamation:";
                if(facing<3)
                    return ":hammer:";
                if(facing<4)
                    return ":crossed_swords:";
                if(facing<5)
                    return ":first_quarter_moon:";
                return ":full_moon:";
            }).join(" ");            
        }

        function roll_dice(quantity, diceType) {
            if(!quantity)
                return "You must specify a number of dice to roll within 1 and 100.";
            if(diceType === "spell")
                return roll_spell_dice(quantity);
            if(diceType === "defense")
                return roll_defense_dice(quantity);
            return roll_attack_dice(quantity);
        }

        return {
            "roll": roll_dice,
            "name": "Warhammer Underworlds",
            "description": "Dice system for Warhammer Underworlds."
        };
    })();

    var gygaxian = (function() {
        function die(sides) {
            return math.floor((math.random() * sides) + 1)
        }

        function sum(sequence) {
            return sequence.reduce(function(left, right) {
                return left+right;
            });
        }

        function signed(num) {
            if(num===0)
                return "";
            if(num>0)
                return `+${num}`;
            return `${num}`;
        }

        function actually_roll_dice(quantity, sides, bonus) {
            var i;
            var accumulated = [];
            for(i=0; i<quantity; ++i) {
                accumulated.push(die(sides));
            }
            return `${quantity}d${sides}${signed(bonus)}: `+
                `**${sum(accumulated)+bonus}** = ${accumulated.join("+")}` +
                `${signed(bonus)}`;
        }

        function roll_dice(quantity, sides, bonus) {
            return actually_roll_dice(
                rng1to1000(quantity),
                rng2to1000(sides),
                bonus || 0);
        }

        return {
            "roll": roll_dice,
            "name": "Gygaxian",
            "description": "The array of polyhedrals typically used in D&D."
        };
    })();

    var surge = (function() {
        var dieFaces = [
            {"val": -1, "happy": 0, "sad": 1, "depict": "_-1_ ☹"},
            {"val":  0, "happy": 0, "sad": 0, "depict": "0"},
            {"val":  0, "happy": 1, "sad": 0, "depict": "**0** ☺"},
            {"val":  1, "happy": 1, "sad": 0, "depict": "**1** ☺"},
            {"val":  2, "happy": 0, "sad": 0, "depict": "2"},
            {"val":  3, "happy": 1, "sad": 1, "depict": "**_3!_** ☺ ☹"},
        ];

        function random() {
            return math.floor(math.random()*6);
        }

        function depict(die) {
            return die.depict;
        }

        function is_happy(die) {
            return die.happy > 0;
        }

        function is_sad(die) {
            return die.sad > 0;
        }


        function surge_roll_dice(quantity) {
            var toRoll = quantity;
            var sum = 0;
            var accumulated = [];
            while(toRoll>0) {
                var face = dieFaces[random()];
                if(face.val!=3)
                    toRoll--;
                sum+=face.val;
                accumulated.push(face);
            }
            accumulated.sort(function(left, right) {
                return right.val - left.val;
            });
            return `${sum} = ${accumulated.map(depict).join(", ")} `;
        }

        function roll_dice(quantity) {
            return surge_roll_dice(rng1to1000(quantity));
        }

        return {
            "roll": roll_dice,
            "name": "Surge Dice",
            "description": "The Surge Dice system.",
        };
    })();

    var fiendWake = (function() {
        function random() {
            return 1+math.floor(math.random()*12);
        }

        function next_stack_number(num) {
            if(num<2)
                return 1;
            return (num%2 === 0) ? num-1 : num-2;
        }

        function can_stack(roll, stackOn, len, lim) {
            return roll===1 || (roll<10 && roll<=stackOn && len<=lim);
        }

        function fiend_wake_roll(originalStackNumber, stackLimit) {
            var tossed = [];
            var proceed = true;
            var stackOn = originalStackNumber;
            while(proceed) {
                var tossedVal = random();
                tossed.push(tossedVal);
                proceed = can_stack(tossedVal,
                                    stackOn,
                                    tossed.length,
                                    stackLimit);
                stackOn = next_stack_number(stackOn);
            }

            return `FW ${originalStackNumber}/${stackLimit}: **${sumup(tossed)}** = ${tossed.join("+")}`;
        }

        function fw_roll(stackNumber, maxStacks) {
            return fiend_wake_roll(stackNumber || 1, maxStacks || 1);
        }
        return {
            "roll": fw_roll,
            "name": "Fiend Wake",
            "description": "The Fiend Wake Stacking Dice system",
        };
    })();

    return module.exports = {
        "cd8": cosmoforceD8,
        "fw": fiendWake,
        "gy": gygaxian,
        "mg": moonsGrave,
        "rk": l5rRollKeepSystem,
        "surge": surge,
        "wu": warhammerUnderworlds,
    };
})(module, console, Math));
